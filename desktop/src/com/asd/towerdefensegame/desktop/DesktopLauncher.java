package com.asd.towerdefensegame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.asd.towerdefensegame.MyTowerDefenseGame;
import com.badlogic.gdx.graphics.g3d.particles.batches.BillboardParticleBatch;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Tower Defense";
		config.width = 960;
		config.height = 540;
		new LwjglApplication(MyTowerDefenseGame.getInstance(), config);
	}
}
