package com.asd.towerdefensegame.Model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by 06k1103 on 07.05.2016.
 */
public class Tower extends Actor{
    TextureRegion img;

    public Tower(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x - width / 2, y - height / 2);
        setSize(width, height);
    }

    public Tower(TextureRegion img, float x, float y) {
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }

    public Tower(Texture img, float x, float y) {
        this(new TextureRegion(img), x, y, img.getWidth(), img.getHeight());
    }

    public Tower(Texture img, float x, float y, float width, float height) {
        this(new TextureRegion(img), x, y, width, height);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
    }
    public void act(float delta)
    {
        for (Actor a:getStage().getActors())
        {
            if (a.getName()!=null && a.getName().equals("knight")){
                if (((Creeps)a).getRectangle().overlaps(getRectangle())){
                    //moveBy((getX()-a.getY())*delta,(getY()-a.getY())*delta);
                    getStage().addActor(new Ammo(new Texture("android/assets/bomb.png"),getX()+25,getY()+25,a.getX()-getX(),a.getY()-getY()));

                }


            }
        }
    }
    public Rectangle getRectangle(){
        return new Rectangle(getX()-50,getY()-50,getWidth()+100,getHeight()+100);
    }

}

