package com.asd.towerdefensegame.Model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by 06k1103 on 30.04.2016.
 */
public class Ammo extends Actor {
    TextureRegion img;
    private float velocityX=10;
    private float velocityY=10;

    public Ammo(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x - width / 2, y - height / 2);
        setSize(width, height);
    }

    public Ammo(TextureRegion img, float x, float y) {
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }

    public Ammo(Texture img, float x, float y) {
        this(new TextureRegion(img), x, y, img.getWidth(), img.getHeight());
    }
    public Ammo(Texture img, float x, float y, float velocityX, float velocityY) {
        this(new TextureRegion(img), x, y, img.getWidth(), img.getHeight());
        this.velocityX = velocityX;
        this.velocityY = velocityY;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
    }

    public void act(float delta){
        moveBy(velocityX*delta,velocityY*delta);
    }
    public void setVelocity(float x, float y){
        velocityX = x;
        velocityY = y;

    }
}


