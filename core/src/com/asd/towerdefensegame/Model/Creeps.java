package com.asd.towerdefensegame.Model;


import com.asd.towerdefensegame.MyTowerDefenseGame;
import com.asd.towerdefensegame.View.ImageActor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by 06k1103 on 23.04.2016.
 */
public class Creeps extends Actor {
    private final float ppuX= MyTowerDefenseGame.getInstance().getPpuX();
    private final float ppuY=MyTowerDefenseGame.getInstance().getPpuY();
    private final float WIDTH=50f;
    private final float HEIGHT=50f;
    private TextureRegion img;
    private TextureRegion knight = new TextureRegion(new Texture("android/assets/knight.png"));
    public Creeps(TextureRegion knight, float x, float y, float width, float height) {
        setName("knight");
        this.img = knight;
        setPosition(x * ppuX, y * ppuY);
        setSize(width * ppuY, height * ppuY);
    }
    public Creeps(float x, float y) {
        setName("knight");
        this.img = knight;
        setPosition(x * ppuX, y * ppuY);
        setSize(WIDTH * ppuY, HEIGHT * ppuY);
    }

    public void draw(Batch batch, float alpha) {
    batch.draw(img, getX(), getY(), getWidth(), getWidth());

    }
    public void act(float delta){
        moveBy(100 * delta, 0);

    }
    public Rectangle getRectangle(){
        return new Rectangle(getX(),getY(),getWidth(),getHeight());
    }

}






