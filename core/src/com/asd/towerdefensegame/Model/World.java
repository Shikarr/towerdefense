package com.asd.towerdefensegame.Model;


import com.asd.towerdefensegame.Controller.MoveToGameMenu;
import com.asd.towerdefensegame.Controller.Towers;
import com.asd.towerdefensegame.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

/**
 * Created by Дмитрий on 13.02.2016.
 */
public class World extends Stage {
    private ImageActor backGround;
    HashMap<String, TextureRegion> regions;
    HashMap<String, TextureRegion> rregions;
    public World(HashMap<String, TextureRegion> regions,HashMap<String, TextureRegion> rregions, ScreenViewport screenViewport, SpriteBatch batch)
    {
        super(screenViewport, batch);
        this.regions = regions;
        this.rregions = rregions;
        initScene();

    }
    private void initScene() {
        backGround = new ImageActor(new Texture("android/assets/background.png"),
                Gdx.graphics.getWidth()/2,
                Gdx.graphics.getHeight()/2,
                Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight());
        addActor(backGround);
    }
    public void clearTowers(){
        getActors().clear();
        initScene();
    }



}
