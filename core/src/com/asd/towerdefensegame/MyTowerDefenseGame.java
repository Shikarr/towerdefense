package com.asd.towerdefensegame;

import com.asd.towerdefensegame.Screens.GameMenuScreen;
import com.asd.towerdefensegame.Screens.GameScreen;
import com.asd.towerdefensegame.Screens.SettingsScreen;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;

public class MyTowerDefenseGame extends Game {
    public static final float WORLD_WIDTH = 960f;
    public static final float WORLD_HEIGHT = 540f;
    private static MyTowerDefenseGame ourInstance = new MyTowerDefenseGame();

    public static MyTowerDefenseGame getInstance() {
        return ourInstance;
    }

    private MyTowerDefenseGame() {

    }
    private float ppuX;
    private float ppuY;

    private SpriteBatch batch;
    private GameMenuScreen gameMenuScreen;
    private SettingsScreen settingsScreen;
    private GameScreen gameScreen;
    private HashMap<String, TextureRegion> regions;
    private HashMap<String, TextureRegion> rregions;

    @Override
    public void create() {
        ppuX = Gdx.graphics.getWidth()/WORLD_WIDTH;
        ppuY = Gdx.graphics.getWidth()/WORLD_HEIGHT;
        loadGraphics();
        loadingGraphics();
        batch = new SpriteBatch();
        gameMenuScreen = new GameMenuScreen(batch, regions);
        settingsScreen = new SettingsScreen(batch , regions);
        gameScreen = new GameScreen(batch, regions,rregions);
        setScreen(gameMenuScreen);
    }

    private void loadGraphics() {
        regions = new HashMap<String, TextureRegion>();
        Texture buttons = new Texture("android/assets/texture_atlas.png");
        TextureRegion[][] btns = new TextureRegion[4][4];
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                btns[i][j] = new TextureRegion(buttons, 155 * j, 155 * i, 155, 155);
        regions.put("Home", btns[0][0]);
        regions.put("Settings", btns[0][1]);
        regions.put("Play", btns[1][1]);
        regions.put("Rating", btns[1][0]);
        regions.put("Play", btns[1][1]);
        regions.put("Frost", btns[1][2]);
        regions.put("MusicPlus", btns[2][1]);
        regions.put("MusicMinus", btns[2][0]);
        regions.put("Credits", btns[2][2]);
        regions.put("Explosion", btns[3][0]);
        regions.put("Arrow", btns[3][1]);
    }
    private void loadingGraphics() {
        rregions = new HashMap<String, TextureRegion>();
        Texture buttons = new Texture("android/assets/texture_atlas2.png");
        TextureRegion[][] btns = new TextureRegion[4][4];
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                btns[i][j] = new TextureRegion(buttons, 50 * j, 50 * i, 50, 50);
        rregions.put("IceTower", btns[0][0]);
        rregions.put("BombTower", btns[1][0]);
        rregions.put("ArrowTower", btns[2][0]);

    }

    public void moveToGameMenu() {
        setScreen(gameMenuScreen);
    }

    public void moveToSettings() {
        setScreen(settingsScreen);
    }

    public void moveToGame() {
        setScreen(gameScreen);
    }
    public float getPpuX() {
        return ppuX;
    }
    public void setPpuX(float ppuX) {
        this.ppuX = ppuX;
    }
    public float getPpuY() {
        return ppuY;
    }
    public void setPpuY(float ppuY) {
        this.ppuY = ppuY;
    }
}
