package com.asd.towerdefensegame.View;

import com.asd.towerdefensegame.Model.Creeps;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

import javax.xml.soap.Text;

/**
 * Created by Дмитрий on 13.02.2016.
 */
public class ImageActor extends Actor {
    TextureRegion img;

    public ImageActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x - width / 2, y - height / 2);
        setSize(width, height);
    }

    public ImageActor(TextureRegion img, float x, float y) {
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }

    public ImageActor(Texture img, float x, float y) {
        this(new TextureRegion(img), x, y, img.getWidth(), img.getHeight());
    }

    public ImageActor(Texture img, float x, float y, float width, float height) {
        this(new TextureRegion(img), x, y, width, height);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
    }


}
