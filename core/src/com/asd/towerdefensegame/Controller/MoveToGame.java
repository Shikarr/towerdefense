package com.asd.towerdefensegame.Controller;

import com.asd.towerdefensegame.MyTowerDefenseGame;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Дмитрий on 20.02.2016.
 */
public class MoveToGame extends ClickListener {
    public void clicked(InputEvent event, float x, float y)
    {
        MyTowerDefenseGame.getInstance().moveToGame();
    }
}
