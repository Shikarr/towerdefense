package com.asd.towerdefensegame.Controller;

import com.asd.towerdefensegame.Model.Tower;
import com.asd.towerdefensegame.Model.World;
import com.asd.towerdefensegame.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.HashMap;

/**
 * Created by 06k1103 on 16.04.2016.
 */
public class UserInterface extends Stage {
    private SpriteBatch batch;
    private World world;
    private ImageActor toMainMenu;
    private ImageActor arrow;
    private ImageActor frost;
    private ImageActor explosion;
    private Towers towers;
    private int tileSize = 50;
    HashMap<String, TextureRegion> regions;
    HashMap<String, TextureRegion> rregions;

    public UserInterface(HashMap<String, TextureRegion> regions,HashMap<String, TextureRegion> rregions, World world) {
        super();
        initScene(regions);
        this.regions = regions;
        this.rregions = rregions;
        this.world = world;
    }

    private void initScene(HashMap<String, TextureRegion> regions) {
        //50
        //340
        toMainMenu = new ImageActor(regions.get("Home"), 120, 430);
        addActor(toMainMenu);
        toMainMenu.addListener(new MoveToGameMenu());
        arrow = new ImageActor(regions.get("Arrow"), 285, 100);
        arrow.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                towers = Towers.ARROWTOWER;
            }
        });
        addActor(arrow);
        frost = new ImageActor(regions.get("Frost"), 485, 100);
        frost.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                towers = Towers.ICETOWER;
            }
        });
        addActor(frost);
        explosion = new ImageActor(regions.get("Explosion"), 675, 100);
        explosion.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                towers = Towers.EXPLOSIONTOWER;
            }
        });
        addActor(explosion);
        towers = Towers.NONE;
    }

    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        boolean res = super.touchDown(screenX, screenY, pointer, button);
        if (res == true)
            return res;
        switch (towers) {
            case ARROWTOWER:
                System.out.println("Arrow");
                screenX-=screenX%tileSize;
                screenY-=screenY%tileSize;
                world.addActor(new Tower(rregions.get("ArrowTower"), screenX, Gdx.graphics.getHeight() - screenY, tileSize, tileSize));
                break;
            case EXPLOSIONTOWER:
                System.out.println("Explode");
                screenX-=screenX%tileSize;
                screenY-=screenY%tileSize;
                world.addActor(new Tower(rregions.get("BombTower"), screenX, Gdx.graphics.getHeight() - screenY, tileSize, tileSize));
                break;
            case ICETOWER:
                System.out.println("Ice");
                screenX-=screenX%tileSize;
                screenY-=screenY%tileSize;
                world.addActor(new Tower(rregions.get("IceTower"), screenX, Gdx.graphics.getHeight() - screenY, tileSize, tileSize));
                break;
            default:
                System.out.println("NONE");
                break;
        }
        return res;
    }

}
