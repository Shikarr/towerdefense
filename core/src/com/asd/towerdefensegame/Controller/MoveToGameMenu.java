package com.asd.towerdefensegame.Controller;

import com.asd.towerdefensegame.MyTowerDefenseGame;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by 06k1103 on 02.04.2016.
 */
public class MoveToGameMenu extends ClickListener {
    public void clicked(InputEvent event, float x, float y) {
        MyTowerDefenseGame.getInstance().moveToGameMenu();
    }
}


