package com.asd.towerdefensegame.Screens;

import com.asd.towerdefensegame.Controller.MoveToGame;
import com.asd.towerdefensegame.Controller.MoveToSettings;
import com.asd.towerdefensegame.Controller.UserInterface;
import com.asd.towerdefensegame.Model.World;
import com.asd.towerdefensegame.MyTowerDefenseGame;
import com.asd.towerdefensegame.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by Дмитрий on 13.02.2016.
 */
public class GameMenuScreen implements Screen {
    private Stage world;
    private ImageActor backGround;
    private ImageActor play;
    private ImageActor rating;
    private ImageActor settings;
    private ImageActor exit;
    public GameMenuScreen(SpriteBatch batch, HashMap<String, TextureRegion> regions) {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());


        world = new Stage(new ScreenViewport(camera), batch);
        backGround = new ImageActor(new Texture("android/assets/game_menu_screen_bg.png"),
                Gdx.graphics.getWidth()/2,
                Gdx.graphics.getHeight()/2,
                Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight());
        world.addActor(backGround);

        play = new ImageActor(regions.get("Play"), 230, 270);
        play.addListener(new MoveToGame());

        rating = new ImageActor(regions.get("Rating"), 541, 270);

        settings = new ImageActor(regions.get("Settings"), 383, 111);
        settings.addListener(new MoveToSettings());

        exit = new ImageActor(regions.get("Home"), 700, 111);

        world.addActor(play);
        world.addActor(rating);
        world.addActor(settings);
        world.addActor(exit);


    }



    @Override
    public void show() {
        Gdx.input.setInputProcessor(world);
    }

    @Override
    public void render(float delta)
    {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        world.act(delta);
        world.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose()
    {
        world.dispose();
    }

}
