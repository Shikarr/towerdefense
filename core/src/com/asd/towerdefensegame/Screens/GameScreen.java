package com.asd.towerdefensegame.Screens;

import com.asd.towerdefensegame.Controller.MoveToGameMenu;
import com.asd.towerdefensegame.Controller.UserInterface;
import com.asd.towerdefensegame.Model.Creeps;
import com.asd.towerdefensegame.Model.World;
import com.asd.towerdefensegame.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.sql.Time;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by 06k1103 on 02.04.2016.
 */
public class GameScreen implements Screen {
    private World world;
    private ImageActor backGround;
    private UserInterface ui;
    private ImageActor toMainMenu;
    private Timer creeptimer;

    public GameScreen(SpriteBatch batch,  HashMap<String, TextureRegion> regions,HashMap<String, TextureRegion> rregions) {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        world = new World(regions,rregions, new ScreenViewport(camera), batch);
        ui = new UserInterface(regions,rregions, world);
        creeptimer = new Timer();
        installCreepTimer();


    }
    public void installCreepTimer() {
        creeptimer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                world.addActor(new Creeps(20, new Random().nextInt(250) + 10));
            }
        }, 2f, 2f);
        creeptimer.stop();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(ui);
    }

    @Override
    public void render(float delta) {
        creeptimer.start();
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        world.act(delta);
        world.draw();
        ui.act(delta);
        ui.draw();


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        world.clearTowers();
    }

    @Override
    public void dispose() {
        world.dispose();
    }
}


