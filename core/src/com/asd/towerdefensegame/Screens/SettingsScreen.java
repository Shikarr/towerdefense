package com.asd.towerdefensegame.Screens;

import com.asd.towerdefensegame.Controller.MoveToGameMenu;
import com.asd.towerdefensegame.Controller.UserInterface;
import com.asd.towerdefensegame.Model.World;
import com.asd.towerdefensegame.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

/**
 * Created by Дмитрий on 20.02.2016.
 */
public class SettingsScreen implements Screen {
    private Stage world;
    private ImageActor backGround;
    private ImageActor moreSound;
    private ImageActor lessSound;
    private ImageActor information;
    private ImageActor backButton;
    private UserInterface ui;
    public SettingsScreen(SpriteBatch batch,  HashMap<String, TextureRegion> regions) {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        world = new Stage(new ScreenViewport(camera), batch);
        backGround = new ImageActor(new Texture("android/assets/background.png"),
                Gdx.graphics.getWidth()/2,
                Gdx.graphics.getHeight()/2,
                Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight());
        world.addActor(backGround);

        backButton = new ImageActor(regions.get("Home"),120 ,430);
        backButton.addListener(new MoveToGameMenu());
        moreSound = new ImageActor(regions.get("MusicPlus"),285 ,250);
        lessSound = new ImageActor(regions.get("MusicMinus"),485 ,250);
        information = new ImageActor(regions.get("Credits"),675 ,250);
        world.addActor(information);
        world.addActor(backButton);
        world.addActor(moreSound);
        world.addActor(lessSound);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(world);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        world.act(delta);
        world.draw();
        //ui.act(delta);
        //ui.draw();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    }
}
